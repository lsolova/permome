import {Component, OnInit} from 'angular2/core';
import {RouteParams, Router} from 'angular2/router';

import {Account, AccountFactory} from './../accounts/account';
import {Transaction} from './transaction'
import {PermomeService} from './../shared/permome.service';

@Component({
    selector: 'tx-list',
    templateUrl: 'app/transactions/transaction-list.template.html',
    providers: [PermomeService]
})
export class TransactionListComponent implements OnInit {
    public account: Account = AccountFactory.emptyAccount();
    public transactions: Transaction[];

    constructor(
        private _router: Router,
        private _routeParams: RouteParams,
        private _permomeService: PermomeService
    ) {}

    ngOnInit() {
        let accId = this._routeParams.get('accid'),
            accPromise = this._permomeService.getAccount({accId: accId}),
            trxListPromise = this._permomeService.getTransactionsOfAccount({accId: accId});
        accPromise.then(acc => this.account = acc, reason => console.log(reason));
        trxListPromise.then(trxList => this.transactions = trxList, reason => console.log(reason));
    }
}
