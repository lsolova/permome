export enum TransactionType {
    Simple = 0,
    Check = 1,
    Summary = 2
}

export interface Transaction {
    txId: string;
    accId: string;
    txEntryDate: number;
    txAddedDate: number;
    txDescription: string;
    txAmount: number;
    txType: TransactionType;
}

export class TransactionFactory {
    
}