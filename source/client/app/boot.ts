import {bootstrap} from 'angular2/platform/browser';
import {bind, provide} from 'angular2/core';
import {ROUTER_PROVIDERS, LocationStrategy, HashLocationStrategy} from 'angular2/router';

import {AppComponent} from './app.component';
import {PermomeService} from './shared/permome.service';

bootstrap(AppComponent, [
    ROUTER_PROVIDERS,
    PermomeService,
    bind(LocationStrategy).toClass(HashLocationStrategy)
]);