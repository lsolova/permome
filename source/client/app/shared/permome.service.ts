import {Injectable, OnInit} from 'angular2/core';
import {Account} from './../accounts/account';
import {Transaction, TransactionType} from './../transactions/transaction';
import {IDbManager, IDbManagerConfiguration} from './dbmanager';
import {DbManager} from './../shared/indexeddb-dbmanager';

const DB_STORE_ACCOUNTS = 'accounts',
      DB_STORE_TRANSACTIONS = 'transactions',
      DB_INDEX_ACCID = 'accid',
      DB_INDEX_ACCNAME = 'accname',
      DB_INDEX_ORDERING = 'ordering',
      COMPLEX_KEY_SEPARATOR = '-';
let dbManager;

function setAccount(trx, account): Promise<Account> {
    let promise = new Promise(function (resolve, reject) {
        let accStore = trx.objectStore(DB_STORE_ACCOUNTS),
            request = accStore.put(account);
        request.onsuccess = function (e) {
            resolve(e.target.result);
        };
    });
    return promise;
};

function getAccount(trx, filter) {
    var promise = new Promise(function(resolve, reject) {
        var objectStore = trx.objectStore(DB_STORE_ACCOUNTS),
            request = objectStore.get(filter.accId);
        request.onerror = function (event) {
            reject(event);
        };
        request.onsuccess = function (event) {
            resolve(event.target.result);
        };
    });
    return promise;
}

function getAllAccount(trx) {
    let promise = new Promise(function (resolve, reject) {
        let trxStore = trx.objectStore(DB_STORE_ACCOUNTS),
            nameIndex = trxStore.index(DB_INDEX_ACCNAME),
            gettingCursor = nameIndex.openCursor(),
            result: Account[] = new Array();

        gettingCursor.onerror = function (event) {
            reject(event);
        };
        gettingCursor.onsuccess = function (e) {
            var cursor = e.target.result;
            if (cursor) {
                result.push(cursor.value);
                cursor.continue();
            } else {
                resolve(result);
            }
        };
    });
    return promise;
}

function addTransaction(trx, transaction) {
    var promise = new Promise(function (resolve, reject) {
        var sdate = Date.now(),
            trxStore = trx.objectStore(DB_STORE_TRANSACTIONS),
            request;

        transaction.sdate = sdate;
        transaction.ordering = transaction.accId + COMPLEX_KEY_SEPARATOR + transaction.tdate + COMPLEX_KEY_SEPARATOR + sdate;
        request = trxStore.add(transaction);
        request.onsuccess = function (e) {
            resolve(e.target.result);
        };
    });
    return promise;
}

function deleteTransaction(trx, filter) {
    var promise = new Promise(function (resolve, reject) {
        var trxStore = trx.objectStore(DB_STORE_TRANSACTIONS),
            request;

        request = trxStore.delete(filter.trxId);
        request.onsuccess = function (e) {
            resolve(e.target.result);
        };
    });
    return promise;
}

function getTransactionsOfAccount(trx, filter) {
    var promise = new Promise(function (resolve, reject) {
        var trxStore = trx.objectStore(DB_STORE_TRANSACTIONS),
            lowerBound = filter.from || '0',
            upperBound = filter.to || '9',
            accountIdFilter = IDBKeyRange.bound(filter.accId + COMPLEX_KEY_SEPARATOR + lowerBound, filter.accId + COMPLEX_KEY_SEPARATOR + upperBound),
            orderIndex = trxStore.index(DB_INDEX_ORDERING),
            gettingCursor = orderIndex.openCursor(accountIdFilter, 'prev'),
            result: Transaction[] = new Array();

        gettingCursor.onerror = function (event) {
            reject(event);
        };
        gettingCursor.onsuccess = function (e) {
            var cursor = e.target.result;
            if (cursor) {
                result.push(cursor.value);
                cursor.continue();
            } else {
                resolve(result);
            }
        };
    });
    return promise;
}

@Injectable()
export class PermomeService {
    constructor() {
        dbManager = new DbManager({
            dbname: 'PermomeDB',
            dbversion: 1,
            tables: [
                {
                    tableName: DB_STORE_ACCOUNTS,
                    keyPath: 'accId',
                    autoIncrement: true,
                    indexes: [
                        {
                            indexName: DB_INDEX_ACCID,
                            indexPath: 'accId'
                        },
                        {
                            indexName: DB_INDEX_ACCNAME,
                            indexPath: 'accName'
                        }
                    ]
                },
                {
                    tableName: DB_STORE_TRANSACTIONS,
                    keyPath: 'trxId',
                    autoIncrement: true,
                    indexes: [
                        {
                            indexName: DB_INDEX_ORDERING,
                            indexPath: DB_INDEX_ORDERING,
                            unique: true
                        }
                    ]
                }
            ]
        });
    }

    setAccount(account: Account): Promise<Account> {
        return dbManager.runQuery(account, DB_STORE_ACCOUNTS, true, setAccount);
    }
    getAccount(filter = {}): Promise<Account> {
        return dbManager.runQuery(filter, DB_STORE_ACCOUNTS, false, getAccount);
    }
    getAllAccount(): Promise<Account[]> {
        return dbManager.runQuery(undefined, DB_STORE_ACCOUNTS, false, getAllAccount);
    }
    addTransaction(transaction): Promise<string> {
        return dbManager.runQuery(transaction, DB_STORE_TRANSACTIONS, true, addTransaction);
    }
	delTransaction(filter): Promise<Transaction> {
        return dbManager.runQuery(filter, DB_STORE_TRANSACTIONS, true, deleteTransaction);
    }
    getTransactionsOfAccount(filter): Promise<Transaction[]> {
        return dbManager.runQuery(filter, DB_STORE_TRANSACTIONS, false, getTransactionsOfAccount);
    }
    resetApp(): Promise<boolean> {
        return dbManager.deleteDb();
    }
}
