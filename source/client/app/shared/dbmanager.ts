export interface IDbManager {
    runQuery(data: any, objectStoreName: string, writable: boolean, queryFunction: Function) : Promise<any>;
    deleteDb(): Promise<boolean>;
}

export interface IDbManagerConfiguration {
    dbname: string;
    dbversion: number;
    tables?: [{
        tableName: string,
        keyPath: string,
        autoIncrement: boolean,
        indexes?: [{
            indexName: string,
            indexPath: string,
            unique?: boolean
        }]
    }];
}