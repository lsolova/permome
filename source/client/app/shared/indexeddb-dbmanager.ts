import {IDbManager, IDbManagerConfiguration} from './dbmanager';

let dbOpenPromise: Promise<any>,
    dbConfig: IDbManagerConfiguration,
    dbStores = new Array();

function openDb() {
    dbOpenPromise = dbOpenPromise || new Promise(function promiseCallbackOfOpenDb(resolve, reject) {
        let request = indexedDB.open(dbConfig.dbname, dbConfig.dbversion);
        
        request.onupgradeneeded = function doOnUpgrade() {
            var db = request.result;
            
            if (dbConfig.tables) {
                dbConfig.tables.forEach(function prepareTable(tableDescription) {
                    let currentStore = db.createObjectStore(
                        tableDescription.tableName,
                        {
                            keyPath: tableDescription.keyPath,
                            autoIncrement: tableDescription.autoIncrement
                        }
                    );
                    if (tableDescription.indexes) {
                        tableDescription.indexes.forEach(
                            ({indexName, indexPath, unique = false}) => currentStore.createIndex(
                                indexName,
                                indexPath,
                                {unique: unique}
                            )
                        );
                    }
                    dbStores[tableDescription.tableName, currentStore];
                });
            }
        };
        
        request.onsuccess = function () {
            resolve(request.result);
        };
        
        request.onerror = function () {
            reject(request.error);
        };
    });
    return dbOpenPromise;
};

function openTransaction(openedDB, objectStore, writable) {
    var trxPromise = new Promise(function promiseCallbackOfOpenTrx(resolve, reject) {
        var trx;
        if (writable) {
            trx = openedDB.transaction(objectStore, 'readwrite');
        } else {
            trx = openedDB.transaction(objectStore);
        }
        resolve(trx);
    });
    return trxPromise;
}

export class DbManager implements IDbManager {
    constructor(conf: IDbManagerConfiguration) {
        dbConfig = conf;
    }
    
    runQuery(data, objectStore, writable, queryFunction) {
        return openDb()
        .then(function (openedDB) {
            return openTransaction(openedDB, objectStore, writable);
        })
        .then(function (trx) {
            return queryFunction.call(null, trx, data);
        });
    }
    
    deleteDb(): Promise<boolean> {
        let deletePromise = new Promise(function promiseCallbackOfDeleteDb(resolve, reject) {
            let deleteResult = indexedDB.deleteDatabase(dbConfig.dbname);
            deleteResult.onsuccess = function () {
                resolve(true);
            };
            deleteResult.onerror = function () {
                resolve(false);
                console.log(deleteResult.error);
            };
            deleteResult.onblocked = function () {
                openDb().then(function (openedDB) {
                    openedDB.close();
                });
            }
        });
        return deletePromise;
    }
}