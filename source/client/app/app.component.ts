import {Component} from 'angular2/core';
import {RouteConfig, ROUTER_DIRECTIVES, RouterOutlet, Router} from 'angular2/router';

import {PermomeService} from './shared/permome.service';
import {AccountListComponent} from './accounts/account-list.component';
import {AccountEditorComponent} from './accounts/account-editor.component';
import {TransactionListComponent} from './transactions/transaction-list.component';

@Component({
  selector: 'permome-app',
  templateUrl: "app/app.template.html",
  directives: [ROUTER_DIRECTIVES, RouterOutlet],
  providers: [PermomeService]
})
@RouteConfig([
  {
    path: '/accounts',
    name: 'AccountList',
    component: AccountListComponent,
    useAsDefault: true
  },

  {path: '/accounts/:id', name: 'AccountEditor', component: AccountEditorComponent},
  {path: '/transactions/:accid', name: 'TransactionList', component: TransactionListComponent}
])

export class AppComponent {
    constructor(
        private _router: Router,
        private _permomeService: PermomeService
    ) {}
    
    resetApplication() {
        this._permomeService.resetApp().then(() => {
            this._router.navigate(['AccountList']);
        } );
    }
}
