import {Component, OnInit} from 'angular2/core';
import {ROUTER_DIRECTIVES, RouteParams, Router} from 'angular2/router';

import {Account, AccountFactory} from './account';
import {PermomeService} from './../shared/permome.service'

@Component({
    selector: 'acc-editor',
    templateUrl: 'app/accounts/account-editor.template.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [PermomeService]
})
export class AccountEditorComponent implements OnInit {
    public currentAccount: Account = AccountFactory.emptyAccount();

    constructor(
        private _router: Router,
        private _routeParams: RouteParams,
        private _permomeService: PermomeService
    ) {}

    ngOnInit() {
        let accId = this._routeParams.get('id');
        if (accId === 'new') {
            this.currentAccount = AccountFactory.createAccount();
        } else {
            this._permomeService.getAccount({accId: accId})
            .then(
                loadedAccount => {
                    if (loadedAccount) {
                        this.currentAccount = loadedAccount;
                    } else {
                        let createdAccount = AccountFactory.createAccount();
                        this.currentAccount = createdAccount;
                    }
                },
                message => {
                    this.currentAccount = AccountFactory.createAccount();
                }
            );
        }
    }

    saveAccount() {
        let savedAccountPromise = this._permomeService.setAccount(this.currentAccount);
        savedAccountPromise.then(account => this._router.navigate(['AccountList']));
    }
}
