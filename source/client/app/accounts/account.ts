import {IdUtils} from './../shared/id.utils';

export interface Account {
    accId: string;
    accName: string;
    accCurrency: string;
    accBalance: number;
}

export class AccountFactory {
    static createAccount(): Account {
        return {
            accId: IdUtils.generateUUID(),
            accName: '',
            accCurrency: 'HUF',
            accBalance: 0
        }
    }
    
    static emptyAccount(): Account {
        return {
            accId: '',
            accName: '',
            accCurrency: '',
            accBalance: 0
        }
    }
}