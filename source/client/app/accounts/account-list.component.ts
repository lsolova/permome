import {Component, OnInit} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';

import {Account} from './account';
import {PermomeService} from './../shared/permome.service';

@Component({
    selector: 'acc-list',
    templateUrl: 'app/accounts/account-list.template.html',
    directives: [ROUTER_DIRECTIVES],
    providers: [PermomeService]
})
export class AccountListComponent implements OnInit {
    public accounts: Account[];

    constructor(private _permomeService: PermomeService) {}

    ngOnInit() {
        let accPromise = this._permomeService.getAllAccount();
        accPromise.then(accounts => this.accounts = accounts, reason => console.log(reason));
    }
}
