require('./shared/shim');
var fs = require('fs'),
    path = require('path'),
    appDir = path.dirname(require.main.filename),
    appServerFolder = appDir + '/app/appserver', 
    opener,
    appSrv = require(appServerFolder + '/ApplicationServer'),
	runtimeArgs = [],
	conf = {
		appname: 'application'
	};
    
console.log(appDir);

process.argv.forEach(function(val, index, array){
	if (index > 1) {
		runtimeArgs.push(val);
	}
});

conf = JSON.parse(fs.readFileSync(appServerFolder + '/appconf.json','utf8'));
conf.appDir = appDir;
if (runtimeArgs.indexOf('--test') >= 0) {
	conf.testing = true;
}

console.log(conf);
appSrv.init(conf);
appSrv.start();