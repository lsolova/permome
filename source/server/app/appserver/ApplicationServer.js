'use strict';

var express = require('express'),
    fs = require('fs'),
    mustache = require('mustache'),
    path = require('path'),
    app = express(),
    appDir,
    clientDir,
    ipaddress,
    port;

var walk = function(dir, prefix, done) {
    var results = [];
    fs.readdir(dir, function(err, list) {
        if (err) { return done(err); }
        var i = 0;
        (function next() {
            var file = list[i++],
                absoluteFile = dir + '/' + file;
            if (!file) { return done(null, results); }
            fs.stat(absoluteFile, function(err, stat) {
                if (stat && stat.isDirectory()) {
                    walk(absoluteFile, prefix + file + '/', function(err, res) {
                        results = results.concat(res);
                        next();
                    });
                } else
                if(!file.endsWith('.appcache'))
                {
                    results.push(prefix + file);
                    next();
                } else {
                    next();
                }
            });
        })();
    });
};

function createAppcacheFile(appname) {
    walk(clientDir, '', function (err, list) {
            var genDate = Date.now();
            console.log(appDir);
            fs.readFile(appDir + '/app/server/appcache_template.mst',
                    {encoding: 'utf8'}, function(err, data) {
                var template = data,
                    content = {
                        genDate: genDate,
                        cachefiles: list
                    },
                    rendered = mustache.render(template, content);
                fs.writeFile(clientDir + '/' + appname + '.appcache', rendered);
            });
        });
}

module.exports = {
    init: function (conf) {
        appDir = conf.appDir;
        clientDir = path.dirname(appDir) + '/client';
        ipaddress = process.env.OPENSHIFT_NODEJS_IP || conf.ip || '127.0.0.1';
        port = process.env.OPENSHIFT_NODEJS_PORT || conf.port || 8080;
        if (conf.testing !== true) {
            createAppcacheFile(conf.appname);
        }
    },
    start: function() {
        app.use('/', express.static(clientDir));
        app.listen(port, ipaddress);
    }
};