var del = require('del'),
    gulp = require('gulp'),
	jshint = require('gulp-jshint'),
    path = require('path'),
	pjson = require('./package.json'),
    sourcemaps = require('gulp-sourcemaps'),
    tsc = require('gulp-typescript');

gulp.task('default', function () {
	// code for default task
});

gulp.task('clean-dist', function () {
    return del(['dist/*']);
});

gulp.task('copy-static-files', function () {
    var copyList = [
        {   from: ['./source/client/**/*', '!./source/client/**/*.ts'],
            to: './dist/client'},
        {   from: ['./source/server/**', '!./source/server/**/*.ts'],
            to: './dist/server'},
        {   from: './source/shared/**',
            to: './dist/server/shared'}
    ];
    
    copyList.forEach(function copyFiles(copyInfo) {
        gulp.src(copyInfo.from)
            .pipe(gulp.dest(copyInfo.to));    
    });
});

gulp.task('compile-typescript', function () {
    var compileList = [
        {   from: './source/client/**/*.ts',
            to: './dist/client'},
        {   from: './source/server/**/*.ts',
            to: './dist/server'}
    ];
    compileList.forEach(function compileFiles(compileInfo) {
        var tsProject = tsc.createProject('tsconfig.json', {
                typescript: require('typescript')
            });
            
        gulp.src(compileInfo.from)
            .pipe(sourcemaps.init()).pipe(tsc(tsProject))
            .js
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(compileInfo.to));
    });
});

gulp.task('copy-libraries', function () {
	var resolvedPath,
		propname,
		propnames = Object.getOwnPropertyNames(pjson.clientDependencies),
		fileArray,
		i,
		j;
	for (i = 0; i < propnames.length; i++) {
		propname = propnames[i];
		if (pjson.clientDependencies.hasOwnProperty(propname)) {
			if (typeof pjson.clientDependencies[propname] === 'string') {
				fileArray = [pjson.clientDependencies[propname]];
			} else {
				fileArray = pjson.clientDependencies[propname];
			}
			for (j = 0; j < fileArray.length; j++) {
				resolvedPath = './node_modules/' + propname + '/' + fileArray[j];
				gulp.src(resolvedPath).pipe(gulp.dest('./dist/client/assets/lib/' + propname + '/' + path.dirname(fileArray[j])));
				console.log('Library file copied: ' + resolvedPath + '.');	
			}
			
		}
	}
	return;
});

gulp.task('dev-watch', function () {
    gulp.watch('./source/**', ['copy-static-files', 'copy-libraries', 'compile-typescript']);
});
